let TVShow = require("../models/tvshow");

//GET for all tvshows que existe en db
exports.findAllTVShows = function (req, res) {
  TVShow.find(function (err, tvshows) {
    if (err) return res.send(500, err.message);
    res.status(200).jsonp(tvshows);
  });
};

//GET especific tvshow
exports.findTVShowById = function (req, res) {
  TVShow.findById(req.params._id, function (err, tvshow) {
    if (err) return res.send(500, err.message);
    res.status(200).jsonp(tvshow);
  });
};

//POST insert a new tvShow in the db
exports.addTVShow = function (req, res) {
  let tvshow = new TVShow({
    title: req.body.title,
    year: req.body.year,
    country: req.body.country,
    poster: req.body.poster,
    seasons: req.body.seasons,
    genre: req.body.genre,
    summary: req.body.summary,
  });

  tvshow.save(function (err) {
    if (err) return res.send(500, err.message);
    res.status(200).jsonp(tvshow);
  });
};

//PUT update a document that allready exist
exports.updateTVShow = function (req, res) {
  TVShow.findById(req.params._id, function (err, tvshow) {
    tvshow.title = req.body.title;
    tvshow.year = req.body.year;
    tvshow.country = req.body.country;
    tvshow.poster = req.body.poster;
    tvshow.seasons = req.body.seasons;
    tvshow.genre = req.body.genre;
    tvshow.summary = req.body.summary;

    tvshow.save(function(err){
        if(err)
            return res.send(500,err.message);
        res.status(200).jsonp(tvshow); 
    });
  });
};

//DELETE delete a TVshow with a specified id
exports.deleteTBShow = function(req,res){
    TVShow.findById(req.params._id,function(err,tvshow){
        tvshow.remove(function(err){
            if(err)
                return res.send(500,err.message)
            res.status(200);
        })
        
    })
}

