let express = require("express"),
  app = express(),
  http = require("http"),
  server = http.createServer(app),
  bodyParser = require("body-parser"),
  methodOverride = require("method-override"),
  mongoose = require("mongoose");

//require controller
let tvShowController = require("./controllers/tvshows");

// Rutas de la API
let tvShows = express.Router();

tvShows.route("/tvshows")
  .get(tvShowController.findAllTVShows)
  .post(tvShowController.addTVShow);

tvShows.route("/tvshows/:id")
  .get(tvShowController.findTVShowById)
  .put(tvShowController.updateTVShow)
  .delete(tvShowController.deleteTBShow);

//-------------------------------------------------------

//connect mongodb
mongoose.connect("mongodb://localhost/tvshows", function (err, res) {
  if (err) {
    console.log("Error concectin to db: ", err);
  } else {
    console.log("Connected to db");
  }
});
require("./models/tvshow");
//--------------------------------------------------------

//app settings
const port = 3001;
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(tvShows);

//definde endpoints
tvShows.get("/", (req, res) => {
  res.send("hola mundo");
});

tvShows.get("/tvshows", (req,res) => {
  console.log("tvshows::::::",tvshows);
  res.json(tvShows);
});

server.listen(port, () => console.log(`listen on port: ${port}`));
